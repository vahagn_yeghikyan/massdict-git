/**
 * Created by vahagn on 4/17/16.
 */

function addWatcher(scope, tab)
{
    scope.$watch(function(){ return tab.directForm;}, function () {
        if(tab[tab.radioModel].transform) {
            tab[tab.radioModel].forms = tab[tab.radioModel].transform.transform(tab.directForm, tab.noun.animate,tab.extendedForm);
        }
    });
    scope.$watch(function(){ return tab.radioModel;}, function () {
        if(tab[tab.radioModel].transform) {
            tab[tab.radioModel].forms = tab[tab.radioModel].transform.transform(tab.directForm, tab.noun.animate,tab.extendedForm);
        }
    });
    scope.$watch(function(){ return tab.extendedForm;}, function () {
        if(tab[tab.radioModel].transform) {
            tab[tab.radioModel].forms = tab[tab.radioModel].transform.transform(tab.directForm, tab.noun.animate,tab.extendedForm);
        }
    });
     scope.$watch(function(){ return tab[tab.radioModel].animate;}, function () {
        if(tab[tab.radioModel].transform) {
            tab[tab.radioModel].forms = tab[tab.radioModel].transform.transform(tab.directForm, tab.noun.animate,tab.extendedForm);
        }
    });


}

myapp.controller('addwordcontroller', ['$scope', '$sce', '$timeout', function ($scope, $sce, $timeout) {

    $scope.getObjectKeysLength = function(obj)
    {
        return !obj ? 0 :Object.keys(obj).length;
    }

    $scope.trustAsHtml = $sce.trustAsHtml;
    $scope.constants = constants;
    $scope.tabIndex = 0;
    $scope.word = "սեղանից";
    $scope.tabs = [
        {
            radioModel:"noun",
            noun:{animate:false, forms : {} },
            verb:{animate:false, forms : {} },
        },

    ];

    $scope.drawNoun = function(){}
    $scope.armlib = new ArmLib();

    addWatcher($scope,$scope.tabs[0]);




    $scope.createnewtab = function () {
        $scope.tabs.push({});
        $scope.tabs[$scope.tabs.length-1].directForm = $scope.tabs[0].directForm;
        //$scope.setActive($scope.tabs.length - 1);
        $timeout(function () {
            $scope.tabIndex = $scope.tabs.length - 1;
        }, 0);
    }
}]);
