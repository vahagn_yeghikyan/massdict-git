/**
 * Created by vahagn on 2/6/16.
 */
$stateProviderRef = null;
$urlRouterProviderRef = null;

var myapp = angular.module('median', ["ui.router","ui.bootstrap"])


myapp.config(function ($httpProvider, $stateProvider, $urlRouterProvider) {

    $stateProviderRef = $stateProvider;

    $stateProvider.state(
        'index',
        {
            url: '/index',
            abstract: true,
            controller: 'menucontroller',
            templateUrl: '/static/views/main.html'

        }
    );

    $stateProvider.state(
        'addword',
        {
            url: '/addword',
            controller: 'addwordcontroller',
            templateUrl: '/static/views/addword.html',
            abstract: false,
            displayName: 'Նոր բառ',
            parent:'index'
        }
    );

    
    $urlRouterProvider.otherwise('/index/addword')

}
);



function switchLanguage(lang) {
    window.location.pathname = '/' + lang + '/';
    //url = Window.location.href.repl
}


myapp.run(['$http', '$urlRouter', '$rootScope', '$state', '$location',
    function ($http, $urlRouter, $rootScope, $state, $location) {

    }
]);