/**
 * Created by vahagn on 5/8/16.
 */

/**
 * Created by vahagn on 4/20/16.
 */

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
var nounsuffixes = {
    n11: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'ի', //տրական
            ablative: 'ից', //բացառական
            accusative: '',
            instrumental: 'ով',//գործիական
            locative: 'ում' //ներգործական
        },
        plural: {
            nominative: 'եր', //ուղղական
            genetive: '', //սեռական
            dative: 'երի', //տրական
            ablative: 'երից', //բացառական
            accusative: '',
            instrumental: 'երով',//գործիական
            locative: 'երում' //ներգործական
        }
    },
    n12: {
        plural: {
            nominative: 'ներ', //ուղղական
            genetive: '', //սեռական
            dative: 'ների', //տրական
            ablative: 'ներից', //բացառական
            accusative: '', //հայցական
            locative: 'ներում' ,//ներգործական
            instrumental: 'ներով'//գործիական
        }
    },
    n15: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'յի', //տրական
            ablative: 'յից', //բացառական
            accusative: '',
            locative: 'յում', //ներգործական
            instrumental: 'յով'//գործիական
        }
    },
    n15a: {
        plural: {
            nominative: 'յեր', //ուղղական
            genetive: '', //սեռական
            dative: 'յերի', //տրական
            ablative: 'յերից', //բացառական
            accusative: '',
            locative: 'յերում', //ներգործական
            instrumental: 'յերով'//գործիական
        }
    },
    n16: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'վի', //տրական
            ablative: 'վից', //բացառական
            accusative: '',
            locative: 'վում', //ներգործական
            instrumental: 'վով'//գործիական
        }
    },
    n17: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'ու', //տրական
            ablative: 'ուց', //բացառական
            accusative: '',
            locative: 'ում', //ներգործական
            instrumental: 'ով'//գործիական
        }
    },
    n18a: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'ո', //տրական
            ablative: 'ից', //բացառական
            accusative: '',
            locative: 'ում', //ներգործական
            instrumental: 'ով'//գործիական
        }
    },
    n25: {
        plural: {
            nominative: 'իկ', //ուղղական
            genetive: '', //սեռական
            dative: 'կանց', //տրական
            ablative: 'կանցից', //բացառական
            accusative: '', //հայցական
            instrumental: 'կանցով',//գործիական
            locative: 'կանցում' //ներգործական
        }
    },
    n31: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ա', //տրական գարնան
            ablative: 'ից', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'ով'//գործիական գարանով
        }
    },
    n32: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ան', //տրական գարնան
            ablative: 'անից', //բացառական գարմանից
            accusative: '',
            locative: 'անում', //ներգործական գարնանով
            instrumental: 'անով'//գործիական գարանով
        }
    },
    n32a: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ան', //տրական գարնան
            ablative: 'ից', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'ամբ'//գործիական գարանով
        }
    },
    n32b: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ան', //տրական գարնան
            ablative: 'նից', //բացառական գարմանից
            accusative: '',
            locative: 'նում', //ներգործական գարնանով
            instrumental: 'նով'//գործիական գարանով
        }
    },
    n33: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ան', //տրական գարնան
            ablative: 'անից', //բացառական գարմանից
            accusative: '',
            locative: 'անում', //ներգործական գարնանով
            instrumental: 'անով'//գործիական գարանով
        }
    },
    n35: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'վա', //տրական գարնան
            ablative: 'վանից', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'ով'//գործիական գարանով
        }
    },
    n41: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական
            dative: 'թյան', //տրական
            ablative: 'թյունից', //բացառական
            accusative: '',
            locative: 'թյունում', //ներգործական
            instrumental: 'թյամբ'//գործիական
        }
    },
    n42: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'վան', //տրական գարնան
            ablative: 'վանից', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'վամբ'//գործիական գարանով
        }
    },
    n51: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'վա', //տրական գարնան
            ablative: 'վանից', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'ով'//գործիական գարանով
        }
    },
    n61: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ոջ', //տրական գարնան
            ablative: 'ոջից', //բացառական գարմանից
            accusative: '',
            locative: 'ոջում', //ներգործական գարնանով
            instrumental: 'ոջով'//գործիական գարանով
        }
    },
    n63a: {
        plural: {
            nominative: 'այք', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'անց', //տրական գարնան
            ablative: 'անցից', //բացառական գարմանից
            accusative: '',
            locative: 'անցում', //ներգործական գարնանով
            instrumental: 'անցով'//գործիական գարանով
        }
    },
    n71: {
        singular: {
            nominative: '', //ուղղական
            genetive: '', //սեռական գարնան
            dative: 'ո', //տրական գարնան
            ablative: 'ուց', //բացառական գարմանից
            accusative: '',
            locative: 'ում', //ներգործական գարնանով
            instrumental: 'ով'//գործիական գարանով
        }
    }
}
var verbsuffixes = {
    'v11': {
        aorist: {
            singular: {
                person1: 'եցի',
                person2: 'եցիր',
                person3: 'եց'
            },
            plural: {
                person1: 'եցինք',
                person2: 'եցիք',
                person3: 'եցին'
            }
        },
        future: {
            singular: {
                person1: 'եմ',
                person2: 'ես',
                person3: 'ի'
            },
            plural: {
                person1: 'ենք',
                person2: 'եք',
                person3: 'են'
            }
        },
        futurePerfect: {
            singular: {
                person1: 'եի',
                person2: 'եիր',
                person3: 'եր'
            },
            plural: {
                person1: 'եինք',
                person2: 'եիք',
                person3: 'եին'
            }
        },
        imperative: {
            singular: {
                person2: 'իր'
            },
            plural: {
                person2: 'եք'
            }
        }
    },
    v11a:{
        imperative: {
            singular: {
                person2: ''
            },
            plural: {
                person2: 'եք'
            }
        }
    },
    v11c:
    {
        aorist: {
            singular: {
                person1: 'ացի',
                person2: 'ացիր',
                person3: 'աց'
            },
            plural: {
                person1: 'ացինք',
                person2: 'ացիք',
                person3: 'ացին'
            }
        }
    },
    'v12': {
        aorist: {
            singular: {
                person1: 'ա',
                person2: 'ար',
                person3: 'ավ'
            },
            plural: {
                person1: 'անք',
                person2: 'աք',
                person3: 'ան'
            }
        }
    },
    v14: {
        imperative: {
            singular: {
                person2: 'ու'
            },
            plural: {
                person2: 'եք'
            }
        }
    },
    v14a: {
        imperative: {
            singular: {
                person2: 'ա'
            },
            plural: {
                person2: 'եք'
            }
        }
    },
    v14b: {
        imperative: {
            singular: {
                person2: 'իր'
            },
            plural: {
                person2: 'րեք'
            }
        }
    },
    'v21': {
        aorist: {
            singular: {
                person1: 'ի',
                person2: 'իր',
                person3: ''
            },
            plural: {
                person1: 'ինք',
                person2: 'իք',
                person3: 'ին'
            }
        },
        future: {
            singular: {
                person1: 'ամ',
                person2: 'աս',
                person3: 'ա'
            },
            plural: {
                person1: 'անք',
                person2: 'աք',
                person3: 'ան'
            }
        },
        futurePerfect: {
            singular: {
                person1: 'այի',
                person2: 'այիր',
                person3: 'ար'
            },
            plural: {
                person1: 'այինք',
                person2: 'այիք',
                person3: 'ային'
            }
        },
        imperative: {
            singular: {
                person2: 'ա'
            },
            plural: {
                person2: 'ացեք'
            }
        }
    },
    v32b: {
        imperative: {
            singular: {
                person2: 'ուր'
            },
            plural: {
                person2: 'վեք'
            }
        }
    }


}
var constants =
{
    letters: 'աբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքևօֆ',
    capitLLetters: 'ԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖ',
    alphabet: this.capitLLetters + this.letters,
    vowels: 'աեէըիոօ',
    consonants: 'բգդզթժլխծկհձղճմյնշչպջռսվտրցփքֆ',
    cases: {
        nominative: {
            name: 'Ուղղական',
            caseCodesingular: 1,
            caseCodeplural: 128,
        }, genetive: {
            name: 'Սեռական',
            caseCodesingular: 2,
            caseCodeplural: 256
        }, dative: {
            name: 'Տրական',
            caseCodesingular: 4,
            caseCodeplural: 512
        }, accusative: {
            name: 'Հայցական',
            caseCodesingular: 16,
            caseCodeplural: 2048
        }, ablative: {
            name: 'Բացառական',
            caseCodesingular: 8,
            caseCodeplural: 1024
        }, instrumental: {
            name: 'Գործիական',
            caseCodesingular: 32,
            caseCodeplural: 4096
        }, locative: {
            name: 'Ներգոյական',
            caseCodesingular: 64,
            caseCodeplural: 8192
        }
    },
    moods: {
        indicative: {
            name: 'Սահմանական',
            tenses: {
                present: 'Անկատար ներկա',
                imperfect: 'Անկատար անցյալ',
                future: 'Ապակատար ներկա',
                futurePerfect: 'Ապակատար անցյալ',
                presentPerfect: 'Վաղակատար ներկա',
                pluperfect: 'Վաղակատար անցյալ',
                aorist: 'Անցյալ կատարյալ',

            }
        },
        subjunctive: {
            name: 'Ըղձական',
            tenses: {
                future: 'Ըղձական ապառնի',
                futurePerfect: 'Ըղձական անցյալ',
            }
        },
        conditional: {
            name: 'Պայմանական',
            tenses: {
                future: 'Պայմանական ապառնի',
                futurePerfect: 'Պայմանական անցյալ',
            }
        },
        imperative: {
            name: 'Հրամայական',
            tenses: {
                '': ''
            }

        }
    },
    participles: {
        infinitive: {
            name: 'Անորոոշ դերբայ',
        },
        passive: {
            name: 'Կրավորական դերբայ',
        },
        causative: {
            name: 'Պատճառական դերբայ',
        },
        aoristStem: {
            name: 'Aorist stem',
        },
        resultative: {
            name: 'Հարակատար դերբայ',
        },
        subject: {
            name: 'Երնթակայական դերբայ',
        },
        imperfective: {
            name: 'Անկատար դերբայ',
        },
        simultaneous: {
            name: 'Համակատար դերբայ',
        },
        perfective: {
            name: 'Վաղակատար դերբայ',
        },
        future1: {
            name: 'Ապառնի դերբայ I',
        },
        future2: {
            name: 'Ապառնի դերբայ II',
        },
        connegative: {
            name: 'Ժխտական դերբայ',
        },

    },
    aoristFlags: {
        addAcEc: 1,
        dropRootLastConsonant: 2,
        addr: 4,
        replaceLastWithC: 8,
    },
    numbers: {
        singular: 'Եղակի', 
        plural: 'Հոգնակի'
    },
    persons: ['person1','person2','person3'],
    auxilliary: {
        indicative: {
            present: {
                singular: {
                    person1: 'եմ',
                    person2: 'ես',
                    person3: 'է'
                },
                plural: {
                    person1: 'ենք',
                    person2: 'եք',
                    person3: 'են'
                },
            },
            imperfect: {
                singular: {
                    person1: 'էի',
                    person2: 'էիր',
                    person3: 'էր'
                },
                plural: {
                    person1: 'էինք',
                    person2: 'էիք',
                    person3: 'էին'
                },
            }
        }
    },
    auxilliaryNegative: {
        indicative: {
            present: {
                singular: {
                    person1: 'չեմ',
                    person2: 'չես',
                    person3: 'չի'
                },
                plural: {
                    person1: 'չենք',
                    person2: 'չեք',
                    person3: 'չեն'
                },
            },
            imperfect: {
                singular: {
                    person1: 'չէի',
                    person2: 'չէիր',
                    person3: 'չէր'
                },
                plural: {
                    person1: 'չէինք',
                    person2: 'չէիք',
                    person3: 'չէին'
                },
            }
        }
    }
}
constants.shortcuts = {
    allCases: 0,
    allSingular: 0,
    allPlural: 0
}
for (var _case in constants.cases) {
    if (_case != 'nominative') {
        constants.shortcuts.allCases = constants.shortcuts.allCases | constants.cases[_case].caseCodesingular | constants.cases[_case].caseCodeplural;

        constants.shortcuts.allSingular = constants.shortcuts.allSingular | constants.cases[_case].caseCodesingular;

        constants.shortcuts.allPlural = constants.shortcuts.allPlural | constants.cases[_case].caseCodeplural;
    }
}

function applyInflection(word, extended, inflection) {

    extended = extended || word;
    if (inflection.type == 'vowel') {
        var pattern = '(ո|ու|[աեէըիօ])([' + constants.consonants + ']*)$'
    }
    else if (inflection.type == 'vowely') {
        var pattern = '(ո|ու|[աեէըիօ])յ([' + constants.consonants + ']*)$'
    }
    else if (inflection.type == 'tyun') {
        var pattern = '(թյուն)()$'
    }
    else if (inflection.type == 'an') {
        var pattern = '(?:ո|ու|[աեէըիօ])([' + constants.consonants + ']*)()$'
    }

    return {
        word: word.replace(new RegExp(pattern), inflection.repl + '$2'),
        extended: extended.replace(new RegExp(pattern), inflection.replext + '$2')
    }

}

function createNountTranformation(name, config) {
    var self = {config: config, name: name};
    self.transform = function (word, animate, extended) {
        extended = extended || word

        var nomSyllables = extended.split(/[\-֊]/g);
        extended = nomSyllables.pop();
        var extendedPrefix = nomSyllables.join('-');
        var result = {
            singular: {},
            plural: {},
            singularHyphenated: {},
            pluralHyphenated: {}
        }
        for (var number in constants.numbers) {
            
            for (var _case in constants.cases) {
                var w = word;
                var e = extended;

                for (var i = 0; i < self.config.inflections.length; i++) {
                    if (self.config.inflections[i].apply & constants.cases[_case]['caseCode' + number]) {
                        var r = applyInflection(w, e, self.config.inflections[i]);
                        w = r.word;
                        e = r.extended;
                    }
                }
                result[number][_case] = w + nounsuffixes[self.config[number + 'Suffixes']][number][_case];
                result[number + 'Hyphenated'][_case] = (extendedPrefix ? (extendedPrefix + '-') : '') + hyphenate(e + nounsuffixes[self.config[number + 'Suffixes']][number][_case]);
            }


            result[number].genetive = result[number].dative;
            result[number+ 'Hyphenated'].genetive = result[number+ 'Hyphenated'].dative;

            if (animate) {
                result[number]['accusative'] = result[number]['dative'];
                result[number]['locative'] = '';
                result[number + 'Hyphenated']['accusative'] = result[number + 'Hyphenated']['dative'];
                result[number + 'Hyphenated']['locative'] = '';

            }
            else {
                result[number]['accusative'] = result[number]['nominative'];
                result[number + 'Hyphenated']['accusative'] = result[number + 'Hyphenated']['nominative'];
            }


            var possesivecases = ['nominative','dative', 'ablative','instrumental'];
            if(!animate)
            {
                possesivecases.push('locative');
            }

            for(var i = 0; i< possesivecases.length; i++) {
                var pcase = possesivecases[i];

                var extCorrection = '';
                if (constants.consonants.indexOf(result[number][pcase][result[number][pcase].length - 1]) != -1) {
                    extCorrection = 'ը';
                }
                result[number][pcase + 'def'] = {
                    person1: result[number][pcase] + 'ս',
                    person2: result[number][pcase] + 'դ',
                }

                result[number + 'Hyphenated'][pcase + 'def'] = {
                    person1: hyphenate(result[number + 'Hyphenated'][pcase] + extCorrection + 'ս'),
                    person2: hyphenate(result[number + 'Hyphenated'][pcase] + extCorrection + 'դ')
                }
            }
            result[number].nominativedef.person3 = [result[number].nominative + 'ն'];
            result[number + 'Hyphenated'].nominativedef.person3 = [result[number+ 'Hyphenated'].nominative + 'ն'];
            result[number].dativedef.person3 = [result[number].dative + 'ն'];
            result[number+ 'Hyphenated'].dativedef.person3 = [result[number+ 'Hyphenated'].dative + 'ն'];
            if(constants.consonants.indexOf(result[number].nominative[result[number].nominative.length - 1]) != -1)
            {
              result[number].nominativedef.person3.push(result[number].nominative + 'ը');
              result[number+ 'Hyphenated'].nominativedef.person3.push(hyphenate(result[number+ 'Hyphenated'].nominative + 'ը'));
            }
            if(constants.consonants.indexOf(result[number].dative[result[number].dative.length - 1]) != -1)
            {
              result[number].dativedef.person3.push(result[number].dative + 'ը');
              result[number+ 'Hyphenated'].dativedef.person3.push(hyphenate(result[number+ 'Hyphenated'].dative + 'ը'));
            }
        }

        return result;
    }
    return self;

}

function N11() {
    return createNountTranformation('N11 բան',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n11'
        }
    );
}

function N12() {
    return createNountTranformation('N12 սեղան',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n12'
        }
    );
}

function N13() {
    return createNountTranformation('N13 սունկ',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allCases | constants.cases.nominative.caseCodeplural
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n11'
        }
    );
}

function N13a() {
    return createNountTranformation('N13a մեջ',
        {
            inflections: [{
                type: 'vowel',
                repl: 'ի',
                replext: 'ի',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n11'
        }
    );
}

function N14() {
    return createNountTranformation('N14 ծաղիկ',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n12'
        }
    );
}

function N15() {
    return createNountTranformation('N15 արաբա',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n15',
            pluralSuffixes: 'n12'
        }
    );
}

function N15a() {
    return createNountTranformation('N15 ա',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n15',
            pluralSuffixes: 'n15a'
        }
    );
}

function N16() {
    return createNountTranformation('N16 կատու',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n16',
            pluralSuffixes: 'n12'
        }
    );
}

function N16a() {
    return createNountTranformation('N16a ձու',
        {
            inflections: [{
                type: 'vowel',
                repl: 'վ',
                replext: 'ըվ',
                apply: constants.shortcuts.allCases | constants.cases.nominative.caseCodeplural
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n11'
        }
    );
}

function N17() {
    return createNountTranformation('N17 կաշի',
        {
            inflections: [{
                type: 'vowel',
                repl: 'վ',
                replext: 'վ',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n12'
        }
    );
}

function N18() {
    return createNountTranformation('N18 բույն',
        {
            inflections: [{
                type: 'vowely',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allCases | constants.cases.nominative.caseCodeplural
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n11'
        }
    );
}

function N18a() {
    return createNountTranformation('N18a հույս',
        {
            inflections: [{
                type: 'vowely',
                repl: 'ու',
                replext: 'ու',
                apply: constants.cases.dative.caseCodesingular | constants.cases.instrumental.caseCodesingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n18a',
            pluralSuffixes: 'n11'
        }
    );
}

function N22() {
    return createNountTranformation('N22 ամուսին',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n17',
            pluralSuffixes: 'n12'
        }
    );
}

function N23() {
    var result = N22();
    result.name = 'N23 գինի';
    return result;
}

function N23a() {
    var result = N22();
    result.name = 'N23a սիրուհի';
    return result;
}

function N24() {
    return createNountTranformation('N24 ձի',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n17',
            pluralSuffixes: 'n11'
        }
    );
}

function N25() {
    return createNountTranformation('N25 մարդ',
        {
            inflections: [], //func: function(word, extended){}, apply
            singularSuffixes: 'n17',
            pluralSuffixes: 'n25'
        }
    );
}

function N26() {
    var result = N22();
    result.name = 'N26 երեխա';
    return result;
}

function N31() {
    return createNountTranformation('N31 ակ',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n31',
            pluralSuffixes: 'n11'
        }
    );
}

function N31a() {
    return createNountTranformation('N31a գառ',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n32b',
            pluralSuffixes: 'n12'
        }
    );
}

function N32() {
    return createNountTranformation('N32 գարուն',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n32',
            pluralSuffixes: 'n12'
        }
    );
}

function N32a() {
    return createNountTranformation('N32a ծագում',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.cases.dative.caseCodesingular | constants.cases.genetive.caseCodesingular | constants.cases.instrumental.caseCodesingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n32a',
            pluralSuffixes: 'n12'
        }
    );
}

function N32b() {
    return createNountTranformation('N32b մուկ',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allCases | constants.cases.nominative.caseCodeplural
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n32b',
            pluralSuffixes: 'n12'
        }
    );
}

function N33() {
    return createNountTranformation('N33 աղջիկ',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n33',
            pluralSuffixes: 'n12'
        }
    );
}

function N34() {
    return createNountTranformation('N34 հանգիստ',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n11',
            pluralSuffixes: 'n12'
        }
    );
}

function N35() {
    return createNountTranformation('N35 մահ',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n35',
            pluralSuffixes: 'n11'
        }
    );
}

function N41() {
    return createNountTranformation('N41 բնություն',
        {
            inflections: [{
                type: 'tyun',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n41',
            pluralSuffixes: 'n12'
        }
    );
}

function N41a() {
    return createNountTranformation('N41a սյուն',
        {
            inflections: [{
                type: 'an',
                repl: '',
                replext: '',
                apply: constants.cases.dative.caseCodesingular | constants.cases.genetive.caseCodesingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n31',
            pluralSuffixes: 'n11'
        }
    );
}

function N42() {
    return createNountTranformation('N42 անուն',
        {
            inflections: [{
                type: 'an',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular ^ constants.cases.locative.caseCodesingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n42',
            pluralSuffixes: 'n12'
        }
    );
}

function N43() {
    return createNountTranformation('N43 տուն',
        {
            inflections: [{
                type: 'an',
                repl: '',
                replext: '',
                apply: constants.cases.dative.caseCodesingular | constants.cases.genetive.caseCodesingular
            }, {
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allSingular ^ (constants.cases.dative.caseCodesingular | constants.cases.genetive.caseCodesingular)
            }, {
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allPlural | constants.cases.nominative.caseCodeplural
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n31',
            pluralSuffixes: 'n11'
        }
    );
}

function N51() {
    return createNountTranformation('N51 ամառ',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n51',
            pluralSuffixes: 'n12'
        }
    );
}

function N52() {
    return createNountTranformation('N52 ամիս',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n51',
            pluralSuffixes: 'n12'
        }
    );
}

function N53() {
    return createNountTranformation('N53 օր',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n51',
            pluralSuffixes: 'n11'
        }
    );
}

function N54() {
    return createNountTranformation('N54 տարի',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allSingular
            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n51',
            pluralSuffixes: 'n12'
        }
    );
}

function N61() {
    return createNountTranformation('N61 ընկեր',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n12'
        }
    );
}

function N62() {
    return createNountTranformation('N62 քույր',
        {
            inflections: [{
                type: 'vowely',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allSingular

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n11'
        }
    );
}

function N62a() {
    return createNountTranformation('N62a տագր',
        {
            inflections: [],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n11'
        }
    );
}

function N63a() {
    return createNountTranformation('N63a կին',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: 'ը',
                apply: constants.shortcuts.allSingular

            },
                {
                    type: 'vowel',
                    repl: 'ա',
                    replext: 'ա',
                    apply: constants.shortcuts.allPlural | constants.cases.nominative.caseCodeplural

                }],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n63a'
        }
    );
}

function N63() {
    return createNountTranformation('N63 տիկին',
        {
            inflections: [{
                type: 'vowel',
                repl: '',
                replext: '',
                apply: constants.shortcuts.allCases

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n63a'
        }
    );
}

function N64() {
    return createNountTranformation('N64 տեր',
        {
            inflections: [{
                type: 'vowel',
                repl: 'ի',
                replext: 'ի',
                apply: constants.shortcuts.allSingular

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n61',
            pluralSuffixes: 'n11'
        }
    );
}

function N71() {
    return createNountTranformation('N71 հայր',
        {
            inflections: [{
                type: 'vowely',
                repl: 'ո',
                replext: 'ո',
                apply: constants.shortcuts.allSingular

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n71',
            pluralSuffixes: 'n11'
        }
    );
}

function N71a() {
    return createNountTranformation('N71a եղբայր',
        {
            inflections: [{
                type: 'vowely',
                repl: 'ո',
                replext: 'ո',
                apply: constants.shortcuts.allSingular

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n71',
            pluralSuffixes: 'n12'
        }
    );
}

function N92() {
    return createNountTranformation('N92 սեր',
        {
            inflections: [{
                type: 'vowel',
                repl: 'ի',
                replext: 'ի',
                apply: constants.shortcuts.allSingular

            }],//func: function(word, extended){}, apply
            singularSuffixes: 'n71',
            pluralSuffixes: 'n11'
        }
    );
}


var ArmLib = function () {

    var self = this;

    self.noun = {
        transformations: {
            n11: N11(),
            n12: N12(),
            n13: N13(),
            n13a: N13a(),
            n14: N14(),
            n15: N15(),
            n15a: N15a(),
            n16: N16(),
            n16a: N16a(),
            n17: N17(),
            n18: N18(),
            n18a: N18a(),
            n22: N22(),
            n23: N23(),
            n23a: N23a(),
            n24: N24(),
            n25: N25(),
            n26: N26(),
            n31: N31(),
            n31a: N31a(),
            n32: N32(),
            n32a: N32a(),
            n32b: N32b(),
            n33: N33(),
            n34: N34(),
            n35: N35(),
            n41: N41(),
            n41a: N41a(),
            n42: N42(),
            n43: N43(),
            n51: N51(),
            n52: N52(),
            n53: N53(),
            n54: N54(),
            n61: N61(),
            n62: N62(),
            n62a: N62a(),
            n63a: N63a(),
            n63: N63(),
            n64: N64(),
            n71: N71(),
            n71a: N71a(),
            n92: N92(),
        }
    };
    self.verb =
    {
        transformations: {
            v11: V11(),
            v11a: V11a(),
            v11b: V11b(),
            v11c: V11c(),
            v12: V12(),
            v12a: V12a(),
            v12b: V12b(),
            v14: V14(),
            v14a: V14a(),
            v14b: V14b(),
            v14c: V14c(),
            v14d: V14d(),
            v21: V21(),
            v22: V22(),
            v22a: V22a(),
            v32a: V32a(),
            v32b: V32b(),
            v32c: V32c(),


        }
    }

}

function hyphenate(word) {
    word = word.replace(/(\-|֊)+/, '');
    var lastIndex = 0;
    var syllables = [];
    var cumConsonant = '';
    for (var i = 0; i < word.length - 1; i++) {

        if (constants.consonants.indexOf(word [i]) != -1) {

            if (word [i + 1] == 'յ' && i < word.length - 2 && constants.vowels.indexOf(word[i + 2]) != -1) {
                if (i != 0) {
                    syllables.push(cumConsonant + word.substring(lastIndex, i));
                }
                cumConsonant = '';
                lastIndex = i;
                i++;
                continue;
            }

            if (word [i + 1] == 'և' && i < word.length - 2 && constants.vowels.indexOf(word[i + 2]) != -1) {
                syllables.push(word.substring(lastIndex, i));
                syllables.push(cumConsonant + word[i] + 'ե');
                cumConsonant = 'վ';
                i += 2;
                lastIndex = i;
                continue;
            }
            if (constants.vowels.indexOf(word[i + 1]) != -1) {
                if (i == 0) {
                    continue;
                }
                syllables.push(cumConsonant + word.substring(lastIndex, i));
                cumConsonant = '';
                lastIndex = i;
                continue;
            }

        }
        else {
            if (word[i + 1] == 'ւ') {
                i++;
            }
            if (constants.vowels.indexOf(word[i + 1]) != -1) {
                syllables.push(cumConsonant + word.substring(lastIndex, i + 1));
                i++;
                lastIndex = i;
                cumConsonant = '';
                continue;
            }
        }
    }
    if (lastIndex < word.length - 1);
    {
        syllables.push(cumConsonant + word.substring(lastIndex));
    }
    return syllables.join('-');
}

function V11() {
    return createVerbTranformation('V11 գրել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        imperativecode: 'v11',
        imperativeFormsWithAorist: false
    });
}

function V11a() {
    return createVerbTranformation('V11a բերել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        imperativecode: 'v11a',
        imperativeFormsWithAorist: false
    });
}

function V11b() {
    return createVerbTranformation('V11b զարկել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11', // same as V11
        imperativecode: 'v11',
        imperativeFormsWithAorist: false
    });
}

function V11c() {
    return createVerbTranformation('V11c ասել', {
        aoristcode: 'v11c',
        futurecode: 'v11',
        futurePerfectcode: 'v11', // same as V11
        imperativecode: 'v21',
        imperativeFormsWithAorist: false
    });
}
function V12() {
    return createVerbTranformation('V12 ընկնել', {
        aoristcode: 'v12',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v11',
        imperativeFormsWithAorist: true
    });
}
function V12a() {
    return createVerbTranformation('V12a տեսնել', {
        aoristcode: 'v12',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v11a',
        imperativeFormsWithAorist: true
    });
}
function V12b() {
    return createVerbTranformation('V12b ելնել', {
        aoristcode: 'v12',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.dropRootLastConsonant, //same as v12a
        imperativecode: 'v11a',
        imperativeFormsWithAorist: true
    });
}
function V14() {
    return createVerbTranformation('V14 մոտեցնել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.addr | constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v14',
        imperativeFormsWithAorist: true
    });
}
function V14a() {
    return createVerbTranformation('V14a անել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.addr | constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v14a',
        imperativeFormsWithAorist: true
    });
}

function V14b() {
    return createVerbTranformation('V14b դնել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.addr | constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v14b',
        imperativeFormsWithAorist: true,
        dropLastLetterinImperative: 3
    });
}
function V14c() {
    return createVerbTranformation('V14c տանել', {
        aoristcode: 'v12',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.addr | constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v11a',
        imperativeFormsWithAorist: true,
    });
}

function V14d() {
    return createVerbTranformation('V14դ թողնել', {
        aoristcode: 'v11',
        futurecode: 'v11',
        futurePerfectcode: 'v11',
        aoristFlags: constants.aoristFlags.dropRootLastConsonant,
        imperativecode: 'v11a',
        imperativeFormsWithAorist: true,
    });
}

function V21() {
    return createVerbTranformation('V21 կարդալ', {
        aoristcode: 'v21',
        futurecode: 'v21',
        futurePerfectcode: 'v21',
        aoristFlags: constants.aoristFlags.addAcEc,
        imperativecode: 'v21',
        imperativeFormsWithAorist: false,
        subjectFormsWithAorist: true
    });
}

function V22() {
    return createVerbTranformation('V22 վախենալ',
        {
            aoristcode: 'v12',
            futurecode: 'v21',
            futurePerfectcode: 'v21',
            aoristFlags: constants.aoristFlags.replaceLastWithC, /// same as V14
            imperativecode: 'v11',
            imperativeFormsWithAorist: true,

        });
}
function V22a() {
    return createVerbTranformation('V22a լվանալ',
        {
            aoristcode: 'v21',
            futurecode: 'v21',
            futurePerfectcode: 'v21',
            aoristFlags: constants.aoristFlags.replaceLastWithC, /// same as V14
            imperativecode: 'v11a',
            imperativeFormsWithAorist: true,
            dropLastLetterinImperative: 1,
            subjectFormsWithAorist: true
        });
}
function V32a() {
    return createVerbTranformation('V32a գալ',
        {
            aoristcode: 'v12',
            gal : true,
            futurecode: 'v21',
            futurePerfectcode: 'v21',
            aoristFlags: constants.aoristFlags.replaceLastWithC, /// same as V14
            imperativecode: 'v11a',
            imperativeFormsWithAorist: true,
            subjectFormsWithAorist: true
        });
}
function V32b() {
    return createVerbTranformation('V32b տալ',
        {
            aoristcode: 'v11',
            gal : true,
            tal : true,
            futurecode: 'v21',
            futurePerfectcode: 'v21',
            aoristFlags: constants.aoristFlags.replaceLastWithC, /// same as V14
            imperativecode: 'v32b',
            imperativeFormsWithAorist: false,
            subjectFormsWithAorist: true
        });
}function V32c() {
    return createVerbTranformation('V32c լալ',
        {
            aoristcode: 'v11',
            gal : true,
            lal : true,
            futurecode: 'v21',
            futurePerfectcode: 'v21',
            aoristFlags: constants.aoristFlags.replaceLastWithC, /// same as V14
            imperativecode: 'v11a',
            imperativeFormsWithAorist: true,
            subjectFormsWithAorist: true
        });
}



function getParticiples(word, animate, extended, config, exceptions) {
    exceptions = exceptions || {};
    var extWord = /(.+)((?:ա|ե)լ)$/;
    var res = extWord.exec(word);
    var root = res[1];
    var ending = res[2];
    var extroot = extWord.exec(extended)[1];

    var result = {
        aoristStem: exceptions.aorist || root,
        extaoristStem: exceptions.aoristext || extroot,
        root: root,
        extroot: extroot
    };


    if (config.aoristFlags & constants.aoristFlags.dropRootLastConsonant && constants.consonants.indexOf(word[word.length - 1]) != -1) {
        result.aoristStem = root.substring(0, root.length - 1);
        result.extaoristStem = extroot.substring(0, extroot.length - 1);
    }
    if (config.aoristFlags & constants.aoristFlags.addAcEc) {
        result.aoristStem = result.aoristStem + (ending == 'ել' ? 'եց' : 'աց');
        result.extaoristStem = result.extaoristStem + (ending == 'ել' ? 'եց' : 'աց');
    }

    if (config.aoristFlags & constants.aoristFlags.addr) {
        result.aoristStem = result.aoristStem + 'ր';
        result.extaoristStem = result.extaoristStem + 'ր';
    }

    if (config.aoristFlags & constants.aoristFlags.replaceLastWithC) {
        result.aoristStem = result.aoristStem.substring(0, result.aoristStem.length - 1) + 'ց';
        result.extaoristStem = result.extaoristStem.substring(0, result.extaoristStem.length - 1) + 'ց';
    }
    if(config.gal)
    {
        result.aoristStem= 'եկ'
        result.extaoristStem= 'եկ'
    }
    if(config.tal)
    {
        result.aoristStem= 'տվ'
        result.extaoristStem= 'տվ'
    }
    if(config.lal)
    {
        result.aoristStem= 'լաց'
        result.extaoristStem= 'լաց'
    }


    result.imperfective =  result.root + (config.gal ? 'ալիս' : 'ում');
    result.extimperfective = result.extroot + (config.gal ? 'ալիս' : 'ում');

    result.future1 = result.root + ending + 'ու';
    result.extfuture1 = result.extroot + ending + 'ու';

    result.perfective = result.aoristStem + 'ել';
    result.extperfective = result.extaoristStem + 'ել';

    result.infinitive = word;
    result.extinfinitive = extended;

    result.causative = root + (ending == 'ել' ? 'եցնել' : 'ացնել');
    result.extcausative = extended + (ending == 'ել' ? 'եցնել' : 'ացնել');

    result.resultative = result.aoristStem + 'ած';/// (ending == 'ել' ? 'եցնել' : 'ացնել');
    result.extresultative = result.extaoristStem + 'ած';

    result.future2 = result.root + (ending == 'ել' ? 'ելիք' : 'ալիք');
    result.extfuture2 = result.extroot + (ending == 'ել' ? 'ելիք' : 'ալիք');

    result.connegative = result.root + (ending == 'ել' ? 'ի' : 'ա');
    result.extconnegative = result.extroot + (ending == 'ել' ? 'ի' : 'ա');


    result.simultaneous = word + 'իս';
    result.extsimultaneous = extended + 'իս';

    if (config.subjectFormsWithAorist) {
        result.subject = result.aoristStem + 'ող';
        result.extsubject = result.extaoristStem + 'ող';
    }
    else {
        result.subject = root + 'ող';
        result.extsubject = extroot + 'ող';
    }

    result.passive = result.root + 'վ' + ending;
    result.extpassive = result.extroot + 'վ' + ending;

    return result;
}

function createVerbTranformation(name, config) {
    var self = {
        config: config,
        name: name
    };

    self.transform = function (word, animate, extended, exceptions) {
        extended = extended || word;


        var indicativeBaseDict =
        {
            present: {
                base: 'imperfective',
                aux: 'present'
            },
            imperfect: {
                base: 'imperfective',
                aux: 'imperfect'
            },
            future: {
                base: 'future1',
                aux: 'present'
            },
            futurePerfect: {
                base: 'future1',
                aux: 'imperfect'
            },
            presentPerfect: {
                base: 'perfective',
                aux: 'present'
            },
            pluperfect: {
                base: 'perfective',
                aux: 'imperfect'
            }
        }


        var moods = {
            indicative: {},
            indicativeNegative: {},
            indicativeHyphenated: {},
            indicativeNegativeHyphenated: {},
            subjunctive: {},
            subjunctiveNegative: {},
            subjunctiveHyphenated: {},
            subjunctiveNegativeHyphenated: {},
            conditional: {},
            conditionalNegative: {},
            conditionalHyphenated: {},
            conditionalNegativeHyphenated: {}
        };
        var r = getParticiples(word, animate, extended, self.config, exceptions);



        //==========================================Indicative all but aorist ===================================================================
        for (var tense in indicativeBaseDict) {
            moods.indicative[tense] = {singular: {}, plural: {}};
            moods.indicativeHyphenated[tense] = {singular: {}, plural: {}};
            for (var number in constants.auxilliary.indicative[indicativeBaseDict[tense].aux]) {
                for (var person in constants.auxilliary.indicative[indicativeBaseDict[tense].aux][number]) {
                    moods.indicative[tense][number][person] = r[indicativeBaseDict[tense].base] + ' ' + constants.auxilliary.indicative[indicativeBaseDict[tense].aux][number][person];
                    moods.indicativeHyphenated[tense][number][person] = hyphenate(r['ext' + indicativeBaseDict[tense].base]) + ' ' + constants.auxilliary.indicative[indicativeBaseDict[tense].aux][number][person];
                }
            }
        }

        //==========================================Indicative all but aorist negation ============================================================

        for (var tense in indicativeBaseDict) {
            moods.indicativeNegative[tense] = {singular: {}, plural: {}};
            moods.indicativeNegativeHyphenated[tense] = {singular: {}, plural: {}};
            for (var number in constants.auxilliary.indicative[indicativeBaseDict[tense].aux]) {
                for (var person in constants.auxilliary.indicative[indicativeBaseDict[tense].aux][number]) {
                    moods.indicativeNegative[tense][number][person] = constants.auxilliaryNegative.indicative[indicativeBaseDict[tense].aux][number][person] + ' ' + r[indicativeBaseDict[tense].base];
                    moods.indicativeNegativeHyphenated[tense][number][person] = constants.auxilliaryNegative.indicative[indicativeBaseDict[tense].aux][number][person] + ' ' + hyphenate(    r['ext' + indicativeBaseDict[tense].base]);
                }
            }
        }
        // ===================================================End ====================================================================================


        //================================================== Indicative AORIST =======================================================================

        moods.indicative.aorist = {}
        moods.indicativeHyphenated.aorist = {}
        var suffixes = verbsuffixes[config.aoristcode].aorist;

        for (var number in suffixes) {
            moods.indicative.aorist[number] = {};
            moods.indicativeHyphenated.aorist[number] = {};
            for (var person in suffixes[number]) {
                moods.indicative.aorist[number][person] = r.aoristStem + suffixes[number][person];
                moods.indicativeHyphenated.aorist[number][person] = hyphenate(r.extaoristStem + suffixes[number][person]);
            }
        }

        //=================================================  End =====================================================================================


        // ================================================Indicative aorist Negative ================================================================

        moods.indicativeNegative.aorist = {}
        moods.indicativeNegativeHyphenated.aorist = {}

        for (var number in suffixes) {
            moods.indicativeNegative.aorist[number] = {};
            moods.indicativeNegativeHyphenated.aorist[number] = {};
            for (var person in suffixes[number]) {
                moods.indicativeNegative.aorist[number][person] = 'չ' + r.aoristStem + suffixes[number][person];
                moods.indicativeNegativeHyphenated.aorist[number][person] = hyphenate( (constants.consonants.indexOf(r.extaoristStem[0])!=-1 ? 'չը' : 'չ') + r.extaoristStem + suffixes[number][person]);
            }
        }

        //===================================================End ====================================================================================

        suffixes = verbsuffixes[config.futurecode].future;

        moods.subjunctive.future = {};
        moods.subjunctiveHyphenated.future = {};
        moods.conditional.future = {};
        moods.conditionalHyphenated.future = {};


        // ===================================================== Subjunctive and Conditional future =================================================================

        for (var number in suffixes) { // just to have ['singular', 'plural']
            moods.subjunctive.future[number] = {};
            moods.subjunctiveHyphenated.future[number] = {};

            moods.conditional.future[number] = {};
            moods.conditionalHyphenated.future[number] = {};
            for (var person in suffixes[number]) {
                moods.subjunctive.future[number][person] = r.root + suffixes[number][person];
                moods.subjunctiveHyphenated.future[number][person] = hyphenate(r.extroot + suffixes[number][person]);

                moods.conditional.future[number][person] = 'կ' + moods.subjunctive.future[number][person];
                moods.conditionalHyphenated.future[number][person] = hyphenate('կ' + (constants.consonants.indexOf(moods.subjunctiveHyphenated.future[number][person][0]) != -1 ? 'ը' : '') + moods.subjunctiveHyphenated.future[number][person]);

            }
        }

        //===========================================================End ===========================================================================









        // ========================================================Subjunctive and conditional futurePerfect =========================================
        suffixes = verbsuffixes[config.futurePerfectcode].futurePerfect;
        moods.subjunctive.futurePerfect = {}
        moods.subjunctiveHyphenated.futurePerfect = {}
        moods.conditional.futurePerfect = {};
        moods.conditionalHyphenated.futurePerfect = {};
        for (var number in suffixes) {
            moods.subjunctive.futurePerfect[number] = {};
            moods.subjunctiveHyphenated.futurePerfect[number] = {};

            moods.conditional.futurePerfect[number] = {};
            moods.conditionalHyphenated.futurePerfect[number] = {};
            for (var person in suffixes[number]) {
                moods.subjunctive.futurePerfect[number][person] = r.root + suffixes[number][person];
                moods.subjunctiveHyphenated.futurePerfect[number][person] = hyphenate(r.extroot + suffixes[number][person]);

                moods.conditional.futurePerfect[number][person] = 'կ' + moods.subjunctive.futurePerfect[number][person];
                moods.conditionalHyphenated.futurePerfect[number][person] = hyphenate('կ' + (constants.consonants.indexOf(moods.subjunctiveHyphenated.futurePerfect[number][person][0]) != -1 ? 'ը' : '') + moods.subjunctiveHyphenated.futurePerfect[number][person]);
            }
        }

        //=========================================================End ===============================================================================



        //========================================================Subjunctive negation ======================================

        for (var tense in moods.subjunctive) {
            moods.subjunctiveNegative[tense] ={};
            moods.subjunctiveNegativeHyphenated[tense] = {};

            for(var number in moods.subjunctive[tense])
            {
                moods.subjunctiveNegative[tense][number] = {};
                moods.subjunctiveNegativeHyphenated[tense][number] = {};
                for(var person in moods.subjunctive[tense][number])
                {
                    moods.subjunctiveNegative[tense][number][person] = 'չ' + moods.subjunctive[tense][number][person];
                    moods.subjunctiveNegativeHyphenated[tense][number][person] = hyphenate('չ' + (constants.consonants.indexOf(moods.subjunctive[tense][number][person][0]) != -1 ? 'ը' : '') + moods.subjunctiveHyphenated[tense][number][person]);
                }
            }
        }

        //===========================================================End ===========================================================================

        //=====================================================Conditional negation ================================================================
        var indicativeBaseDict =
        {
            future: {
                base: 'connegative',
                aux: 'present'
            },
            futurePerfect: {
                base: 'connegative',
                aux: 'imperfect'
            }
        }



        for (var tense in indicativeBaseDict) {
            moods.conditionalNegative[tense] = {singular: {}, plural: {}};
            moods.conditionalNegativeHyphenated[tense] = {singular: {}, plural: {}};
            for (var number in constants.auxilliary.indicative[indicativeBaseDict[tense].aux]) {
                for (var person in constants.auxilliary.indicative[indicativeBaseDict[tense].aux][number]) {
                    moods.conditionalNegative[tense][number][person] = constants.auxilliaryNegative.indicative[indicativeBaseDict[tense].aux][number][person] + ' ' + r[indicativeBaseDict[tense].base]  ;
                    moods.conditionalNegativeHyphenated[tense][number][person] = constants.auxilliaryNegative.indicative[indicativeBaseDict[tense].aux][number][person] + ' ' + hyphenate(r['ext' + indicativeBaseDict[tense].base]);
                }
            }
        }
        //===========================================================================================================================================



        // = =============================================================Imperative ================================================================
        var simperativeRoot = self.config.imperativeFormsWithAorist ? r.aoristStem : r.root; // singular imperative
        var pimperativeRoot = simperativeRoot;


        var extsimperativeRoot = self.config.imperativeFormsWithAorist ? r.extaoristStem : r.extroot;
        var extpimperativeRoot = extsimperativeRoot;
        if(self.config.dropLastLetterinImperative & 1)
        {
            simperativeRoot = simperativeRoot.substring(0, simperativeRoot.length-1);
            extsimperativeRoot = extsimperativeRoot.substring(0, extsimperativeRoot.length-1);
            if(/([աեէըիոօ]|ու)$/i.exec(extsimperativeRoot))
            {
                extsimperativeRoot = extsimperativeRoot.substring(0, extsimperativeRoot.length-1);
            }
        }
        if(self.config.dropLastLetterinImperative & 2)
        {
            pimperativeRoot = pimperativeRoot.substring(0, pimperativeRoot.length-1);
            extpimperativeRoot = extpimperativeRoot.substring(0, extpimperativeRoot.length-1);

        }

        moods.imperative = {
            '': {
                singular: {
                    person1: '',
                    person2: simperativeRoot + verbsuffixes[self.config.imperativecode].imperative.singular.person2,
                    person3: ''
                },
                plural: {
                    person1: '',
                    person2: pimperativeRoot + verbsuffixes[config.imperativecode].imperative.plural.person2,
                    person3: ''
                }
            }
        }

        moods.imperativeHyphenated = {
            '': {
                singular: {
                    person1: '',
                    person2: hyphenate(extsimperativeRoot + verbsuffixes[config.imperativecode].imperative.singular.person2),
                    person3: ''
                },
                plural: {
                    person1: '',
                    person2: hyphenate(extpimperativeRoot + verbsuffixes[config.imperativecode].imperative.plural.person2),
                    person3: ''
                }
            }
        }
        // ===============================================================End ======================================================================

        //============================================================Imperative negation ==========================================================
        moods.imperativeNegative = {
            '': {
                singular: {
                    person1: '',
                    person2: 'մի ' + moods.imperative[''].singular.person2,
                    person3: ''
                },
                plural: {
                    person1: '',
                    person2: 'մի ' + moods.imperative[''].plural.person2,
                    person3: ''
                }
            }
        }

        moods.imperativeNegativeHyphenated = {
            '': {
                singular: {
                    person1: '',
                    person2: 'մի ' + moods.imperativeHyphenated[''].singular.person2,
                    person3: ''
                },
                plural: {
                    person1: '',
                    person2: 'մի ' + moods.imperativeHyphenated[''].plural.person2,
                    person3: ''
                }
            }
        }
        //==========================================================================================================================================

        return {moods: moods, participles: r};

    }
    return self;

}