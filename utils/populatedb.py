# -*- coding: utf-8 -*-
import MySQLdb
import re

Alphabet = 'աբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքևօֆ'
Alphabet += 'ԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖ'
connection = MySQLdb.connect('localhost','root','123456', 'massdict', charset = 'utf8')

cur = connection.cursor(MySQLdb.cursors.DictCursor)


batch_size = 500

cur.execute("SELECT max(Article_id) as startId FROM massdict.ui_wordoccurence")


start_id = cur.fetchone()['startId']
globalWordDict = {}
query = "SELECT * FROM massdict.ui_rawword"
cur.execute(query)
for w in cur.fetchall():
    globalWordDict[w['Word']] = w['id']



def insertWord(word, articleid):
    cursor = connection.cursor()

    #cursor.execute("SELECT id FROM massdict.ui_rawword WHERE Word = '{0}'".format(word))
    #word_id = cursor.fetchone()
    if word in globalWordDict:
        word_id = globalWordDict[word]
    else:
        cursor.execute("INSERT INTO massdict.ui_rawword(Word) VALUES ('{0}')".format(word))
        globalWordDict[word] = cursor.lastrowid
        word_id = cursor.lastrowid
    cursor.execute("INSERT INTO massdict.ui_wordoccurence(Article_id,Word_id) VALUES({0},{1})".format(articleid, word_id))






wordSplit = re.compile('[^' + Alphabet +']+', re.UNICODE)
clean = re.compile("[՜'՛՟]", re.UNICODE)

while True:
    query = 'SELECT id, Content FROM massdict.ui_article WHERE id > {0} LIMIT {1}'.format(start_id, batch_size)
    print('Selecting next 500 rows from database startting from id = {0}'.format(start_id))
    cur.execute(query)
    if cur.rowcount == 0:
        print('Successfully finished')
        break
    for article in cur.fetchall():

        start_id = article['id']
        content = article['Content']
        if not content:
            continue

        content = clean.sub(' ',content)



        m = wordSplit.split(content)
        for i in range(len(m)):
            m[i] = m[i].lower()

        m = list(set(m))
        for word in m:
            word = word.replace('եւ','և')

            if not word:
                continue
            insertWord(word, start_id)
        connection.commit()



