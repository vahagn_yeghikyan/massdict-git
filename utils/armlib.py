import copy
import re

suffixes = {
    "n11": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ի",  # սեռական
            "dative": "ին",  # տրական
            "ablative": "ից",  # բացառական
            "accusative": "",
            "locative": "ում",  # ներգործական
            "instrumental": "ով"  # գործիական
        },
        "plural": {
            "indicative": "եր",  # ուղղական
            "genetive": "երի",  # սեռական
            "dative": "երին",  # տրական
            "ablative": "երից",  # բացառական
            "accusative": "",
            "instrumental": "երով",  # գործիական
            "locative": "երում"  # ներգործական
        }
    },
    "n12": {
        "plural": {
            "indicative": "ներ",  # ուղղական
            "genetive": "ների",  # սեռական
            "dative": "ներին",  # տրական
            "ablative": "ներից",  # բացառական
            "accusative": "",  # հայցական
            "instrumental": "ներով",  # գործիական
            "locative": "ներում"  # ներգործական
        }
    },
    "n15": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "յի",  # սեռական
            "dative": "յին",  # տրական
            "ablative": "յից",  # բացառական
            "accusative": "",
            "locative": "յում",  # ներգործական
            "instrumental": "յով"  # գործիական
        }
    },
    "n16": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "վի",  # սեռական
            "dative": "վին",  # տրական
            "ablative": "վից",  # բացառական
            "accusative": "",
            "locative": "վում",  # ներգործական
            "instrumental": "վով"  # գործիական
        }
    },
    "n17": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ու",  # սեռական
            "dative": "ուն",  # տրական
            "ablative": "ուց",  # բացառական
            "accusative": "",
            "locative": "ում",  # ներգործական
            "instrumental": "ով"  # գործիական
        }
    },
    "n25": {
        "plural": {
            "indicative": "իկ",  # ուղղական
            "genetive": "կանց",  # սեռական
            "dative": "կանց",  # տրական
            "ablative": "կանցից",  # բացառական
            "accusative": "",  # հայցական
            "instrumental": "կանցով",  # գործիական
            "locative": "կանցում"  # ներգործական
        }
    },
    "n32": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ան",  # սեռական գարնան
            "dative": "ան",  # տրական գարնան
            "ablative": "անից",  # բացառական գարմանից
            "accusative": "",
            "locative": "անում",  # ներգործական գարնանով
            "instrumental": "անով"  # գործիական գարանով
        }
    },
    "n32b": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ան",  # սեռական գարնան
            "dative": "ան",  # տրական գարնան
            "ablative": "նից",  # բացառական գարմանից
            "accusative": "",
            "locative": "նում",  # ներգործական գարնանով
            "instrumental": "նով"  # գործիական գարանով
        }
    },
    "n33": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ա",  # սեռական գարնան
            "dative": "ան",  # տրական գարնան
            "ablative": "անից",  # բացառական գարմանից
            "accusative": "",
            "locative": "անում",  # ներգործական գարնանով
            "instrumental": "անով"  # գործիական գարանով
        }
    },
    "n41": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "յան",  # սեռական
            "dative": "յան",  # տրական
            "ablative": "յունից",  # բացառական
            "accusative": "",
            "locative": "յունում",  # ներգործական
            "instrumental": "յունով"  # գործիական
        }
    },
    "n42": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "վան",  # սեռական գարնան
            "dative": "վան",  # տրական գարնան
            "ablative": "վանից",  # բացառական գարմանից
            "accusative": "",
            "locative": "վանում",  # ներգործական գարնանով
            "instrumental": "վամբ"  # գործիական գարանով
        }
    },
    "n51": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "վա",  # սեռական գարնան
            "dative": "վա",  # տրական գարնան
            "ablative": "վանից",  # բացառական գարմանից
            "accusative": "",
            "locative": "ում",  # ներգործական գարնանով
            "instrumental": "ով"  # գործիական գարանով
        }
    },
    "n61": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": "ոջ",  # սեռական գարնան
            "dative": "ոջ",  # տրական գարնան
            "ablative": "ոջից",  # բացառական գարմանից
            "accusative": "",
            "locative": "ոջում",  # ներգործական գարնանով
            "instrumental": "ոջով"  # գործիական գարանով
        }
    },
    "n63a": {
        "plural": {
            "indicative": "այք",  # ուղղական
            "genetive": "անց",  # սեռական գարնան
            "dative": "անց",  # տրական գարնան
            "ablative": "անցից",  # բացառական գարմանից
            "accusative": "",
            "locative": "անցում",  # ներգործական գարնանով
            "instrumental": "անցով"  # գործիական գարանով
        }
    },
    "n71": {
        "singular": {
            "indicative": "",  # ուղղական
            "genetive": " ",  # սեռական գարնան
            "dative": " ",  # տրական գարնան
            "ablative": "ից",  # բացառական գարմանից
            "accusative": "",
            "locative": "ում",  # ներգործական գարնանով
            "instrumental": "ով"  # գործիական գարանով
        }
    }
}

constants = {
    "letters": 'աբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքևօֆ',
    "capitLLetters": 'ԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖ',
    "vowels": 'աեէըիոօ',
    "consonants": 'բգդզթժլխծկհձղճմյնշչպջռսվտրցփքֆ'}


def identityFunction(a):
    return a


def applyNounSuffixes(word, root, suffixes, prefix=None, callback=None):
    result = {}
    callback = callback or identityFunction
    for _case in suffixes:
        result[_case] = (prefix + '-' if prefix else '') + callback(root + suffixes[_case] if suffixes[_case] else word)

    return result


def hyphenate(word):
    lastIndex = 0
    syllables = []
    cumConsonant = ''
    skipCount = 0
    for i in range(len(word) - 1):
        if skipCount:
            skipCount -= 1
            continue

        if word[i] in constants["consonants"]:
            if word[i + 1] == 'յ' and i < len(word) - 2 and (word[i + 2] in constants["vowels"]):
                if i != 0:
                    syllables.append(cumConsonant + word[lastIndex: i])
                    cumConsonant = ''
                    lastIndex = i
                    skipCount += 1
                    continue

            if word[i + 1] == 'և' and i < len(word) - 2 and (word[i + 2] in constants["vowels"]):
                syllables.append(word[lastIndex: i])
                syllables.append(cumConsonant + word[i] + 'ե')
                cumConsonant = 'վ'
                i += 2
                skipCount += 2
                lastIndex = i
                continue
            if word[i + 1] in constants["vowels"]:
                if i == 0:
                    continue

                syllables.append(cumConsonant + word[lastIndex: i])
                cumConsonant = ''
                lastIndex = i
                continue
        else:
            if word[i + 1] == 'ւ':
                skipCount += 1
                i += 1
                if i < len(word) - 1 and word[i + 1] in constants["vowels"]:
                    syllables.append(cumConsonant + word[lastIndex: i + 1])
                    i += 1
                    skipCount += 1
                    lastIndex = i
                    cumConsonant = ''
                    continue
    if lastIndex < len(word) - 1:
        syllables.append(cumConsonant + word[lastIndex:])

    return '-'.join(syllables)


def replaceLastVowel(str, replacement=None):
    replacement = replacement or ''
    vyunCorrection = 0
    for i in range(len(str) - 1, 0, -1):
        if str[i] in constants['vowels']:
            return str[0: i] + replacement + str[i + 1 + vyunCorrection:]
        else:
            vyunCorrection = 1 if str[i] == 'ւ' else 0
    return str


def grlw(replacement):
    def f(word):
        return replaceLastVowel(word, replacement)

    return f


class NounTransformation:
    def __init__(self, name, singularsuffix, pluralsuffix, getRoot=None, getExtendedRoot=None, getPluralRoot=None,
                 getPluralExtendedRoot=None):
        self.name = name
        self.singularSuffixes = copy.deepcopy(suffixes[singularsuffix]['singular'])
        self.plurarSuffixes = copy.deepcopy(suffixes[pluralsuffix]['plural'])
        self.singularSuffixGroupName = singularsuffix
        self.plurarSuffixGroupName = pluralsuffix
        self.getExtendedSingularSuffixes = self.getSingularSuffixes
        self.getExtendedPlurarSufixes = self.getPluralSuffixes
        self.getRoot = getRoot or identityFunction
        self.getExtendedRoot = getExtendedRoot or self.getRoot
        self.getPluralRoot = getPluralRoot or self.getRoot
        self.getPluralExtendedRoot = getPluralExtendedRoot or self.getExtendedRoot

    def getSingularSuffixes(self, animate):
        result = self.singularSuffixes
        if animate:
            result['accusative'] = result['dative']
        else:
            result['accusative'] = result['indicative']
        return result

    def getPluralSuffixes(self, animate):
        result = self.plurarSuffixes
        if animate:
            result['accusative'] = result['dative']
        else:
            result['accusative'] = result['indicative']
        return result

    def transform(self, word, animate=False, extendedForm=None):
        extendedForm = extendedForm or word
        nomSyllables = re.split('[\-֊]', extendedForm)
        lastBlock = nomSyllables.pop()
        root = self.getRoot(word)
        pluralRoot = self.getPluralRoot(word)
        extendedRoot = self.getExtendedRoot(lastBlock)
        extendedRootPlural = self.getPluralExtendedRoot(lastBlock)
        prefix = '-'.join(nomSyllables)
        return {
            "singular": applyNounSuffixes(word, root, self.getSingularSuffixes(animate)),
            "plural": applyNounSuffixes(pluralRoot, pluralRoot, self.getPluralSuffixes(animate)),
            "singularHyphenated": applyNounSuffixes(lastBlock, extendedRoot, self.getExtendedSingularSuffixes(animate),
                                                    prefix,
                                                    hyphenate),
            "pluralHyphenated": applyNounSuffixes(extendedRootPlural, extendedRootPlural,
                                                  self.getExtendedPlurarSufixes(animate),
                                                  prefix, hyphenate)}


def N11():
    return NounTransformation('N11 բան', 'n11', 'n11')


def N12():
    return NounTransformation('N12 սեղան', 'n11', 'n12')


def N13():
    return NounTransformation('N13 սունկ', 'n11', 'n11', replaceLastVowel, grlw('ը'))


def N13a():
    repl1 = grlw('ի')
    return NounTransformation('N13a մեջ', 'n11', 'n11', repl1, repl1, identityFunction, identityFunction)


def N14():
    return NounTransformation('N14 ծաղիկ', 'n11', 'n12', replaceLastVowel, replaceLastVowel, identityFunction,
                              identityFunction)


def N15():
    return NounTransformation('N15 արաբա', 'n15', 'n12')


def N15a():
    return NounTransformation('N15 ա', 'n15', 'n11')


def N16():
    return NounTransformation('N16 կատու', 'n16', 'n12', replaceLastVowel, replaceLastVowel, identityFunction,
                              identityFunction)


def N17():
    return NounTransformation('N17 կասի', 'n17', 'n12', replaceLastVowel, replaceLastVowel, identityFunction,
                              identityFunction)


def N18():
    def repl1(word):
        return re.sub('ույ([' + constants['consonants'] + ']+)$', r'\1', word, re.UNICODE)

    def repl2(word):
        return re.sub('ույ([' + constants['consonants'] + ']+)$', r'ը\1', word, re.UNICODE)

    return NounTransformation('N18 բույն', 'n11', 'n11', repl1, repl2, None, repl2)


def N22():
    return NounTransformation('N22 ամուսին', 'n17', 'n12', replaceLastVowel, None, identityFunction, identityFunction)


def N25():
    return NounTransformation('N25 մարդ', 'n17', 'n25')


def N32():
    return NounTransformation('N32 գարուն', 'n32', 'n12', replaceLastVowel, None, identityFunction)


def N32b():
    return NounTransformation('N32b մուկ', 'n32b', 'n12', replaceLastVowel, grlw('ը'))


def N33():
    return NounTransformation('N33 աղջիկ', 'n33', 'n12', replaceLastVowel, grlw('ը'), identityFunction,
                              identityFunction)


def N34():
    return NounTransformation('N34 հանգիստ', 'n11', 'n12', replaceLastVowel, grlw('ը'), identityFunction,
                              identityFunction)


def N41():
    def repl1(word):
        return re.sub('([' + constants["consonants"] + '])յուն$', r'\1', word, re.UNICODE)

    return NounTransformation('N41 բնություն', 'n41', 'n12', repl1, None, identityFunction, identityFunction)


def N42():
    def repl1(word):
        return re.sub('[' + constants["vowels"] + ']ւ?[' + constants["consonants"] + ']$', r'', word, re.UNICODE)

    return NounTransformation('N42 անուն', 'n42', 'n12', repl1, None, identityFunction, identityFunction)


def N43():
    def repl1(word):
        return re.sub('[' + constants["vowels"] + ']ւ?[' + constants["consonants"] + ']$', r'', word, re.UNICODE)

    def repl2(word):
        return re.sub('[' + constants["vowels"] + ']ւ?[' + constants["consonants"] + ']$', r'ը', word, re.UNICODE)

    result = NounTransformation('N43 տուն', 'n32b', 'n12', repl1, repl1, None, repl2)

    def getExtendedSingularSuffixes(animate):
        r = copy.deepcopy(suffixes[result.singularSuffixGroupName]["singular"])
        r["ablative"] = 'ը' + r["ablative"]
        r["locative"] = 'ը' + r["locative"]
        r["instrumental"] = 'ը' + r["instrumental"]
        return r

    result.getExtendedSingularSuffixes = getExtendedSingularSuffixes
    return result


def N51():
    return NounTransformation('N51 ամառ', 'n51', 'n12')


def N52():
    return NounTransformation('N52 ամիս', 'n51', 'n12', replaceLastVowel, replaceLastVowel, identityFunction,
                              identityFunction)


def N53():
    return NounTransformation('N53 օր', 'n51', 'n11')


def N61():
    return NounTransformation('N61 ընկեր', 'n61', 'n12')


def N62():
    def repl1(word):
        return re.sub('ույ([' + constants["consonants"] + ']+)$', r'\1', word, re.UNICODE)

    def repl2(word):
        return re.sub('ույ([' + constants["consonants"] + ']+)$', r'ը\1', word, re.UNICODE)

    return NounTransformation('N62 քույր', 'n61', 'n11', repl1, repl2, identityFunction, identityFunction)


def N63a():
    return NounTransformation('N63a կին', 'n61', 'n63a', replaceLastVowel, grlw('ը'), grlw('ա'), grlw('ա'))


def N63():
    return NounTransformation('N63 տիկին', 'n61', 'n63a', replaceLastVowel)


def N64():
    return NounTransformation('N64 տեր', 'n61', 'n11', grlw('ի'), None, identityFunction, identityFunction)


def N71():
    def repl1(word):
        return re.sub('այ([' + constants["consonants"] + ']+)$', r'ո\1', word, re.UNICODE)

    return NounTransformation('N71 հայր', 'n71', 'n11', repl1, None, identityFunction, identityFunction)


def N71a():
    def repl1(word):
        return re.sub('այ([' + constants["consonants"] + ']+)$', r'ո\1', word, re.UNICODE)

    return NounTransformation('N71a եղբայր', 'n71', 'n12', repl1, None, identityFunction, identityFunction)


def N73():
    def repl1(word):
        return re.sub('այ([' + constants["consonants"] + ']+)$', r'ո\1', word, re.UNICODE)

    return NounTransformation('N73 սեր', 'n71', 'n12', repl1, None, identityFunction, identityFunction)


NounTransformations = [
    N11(),
    N12(),
    N13(), N13a(), N14(), N15(), N15a(), N16(), N17(), N18(), N41(), N51(), N22(), N25(), N32(), N32b(), N33(), N34(),
    N42(), N43(), N51(), N52(), N53(), N61(), N62(), N63(), N63a()]


