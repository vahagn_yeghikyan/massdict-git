from lxml import html
import os
import re
from utils import armlib

eancDict = {
    'sg': 'singular',
    'pl': 'plural'
}

BASE_DIR = '/home/vahagn/arevak'

dirs = list(
    sorted([directory for directory in os.listdir(BASE_DIR) if os.path.isdir(os.path.join(BASE_DIR, directory))]))
pass

transformDict = {
    'nmlz': ''
}


def getPartPfSpeach(word):
    m = re.search('(\w+)\s*\((\w+)(?:\s+(\w+))?\)', word, re.UNICODE)
    if m:
        return m.group(1), m.group(2), m.group(3)
    else:
        return ("", "", "")


def getPluralSingular(props):
    return 'singular' if re.search(r'(^|[^\w])sg[^\w]', props) else 'plural'


cases = {
    "nom": "nominative",
    "gen": "genitive",
    "dat": "dative",
    "abl": "ablative",
    "ins": "instrumental",
    "loc": "locative"
}


def getCase(word):
    m = re.search('(?:^|[^\w])(' + '|'.join(cases.keys()) + ')+($|[^\w])', word, re.UNICODE)
    if m:
        return cases[m.group(1)]


def getDef(word):
    if re.search('[^\w]poss1($|[^\w])', word):
        return 'person1'
    if re.search('[^\w]poss2($|[^\w])', word):
        return 'person2'
    if re.search('[^\w]def($|[^\w])', word):
        return 'person3'


globalDict = {}

counter = 0
totalCount = float(len(dirs))
for directory in dirs:
    print('processed {0}%', counter / totalCount)
    counter += 1
    directory = os.path.join(BASE_DIR, directory)
    files = sorted(os.listdir(directory))
    for file in files:
        file = os.path.join(directory, file)
        with open(file, 'r') as fileHndl:
            contentStr = fileHndl.read()
            xmlObject = html.fromstring(contentStr)
            words = xmlObject.xpath('//span')
            for word in words:
                wordTxt = word.text.lower()
                wordTxt = re.sub(r'[^աբգդեզէըթժիլխծկհձղճմյնշոչպջռսվտրցւփքևօֆ]+','',wordTxt, re.UNICODE)

                if 'titles' not in word.attrib:
                    continue
                meanings = word.attrib['titles'].split('\n')
                for meaning in meanings:
                    meaningLines = meaning.split('\t')
                    if len(meaningLines) < 3:
                        continue
                    directForm, partOfSpeach, type = getPartPfSpeach(meaningLines[0])
                    if partOfSpeach == 'N':
                        if directForm not in globalDict:
                            globalDict[directForm] = []
                        if wordTxt not in globalDict[directForm]:
                            globalDict[directForm].append(wordTxt)
                            #
                            #     {
                            #         'animate': False,
                            #         'singular': {
                            #
                            #
                            #         }, 'plural': {
                            #
                            #         },
                            #         'alloweddecl': []}
                            #
                            #
                            # number = getPluralSingular(meaningLines[1])
                            # case = getCase(meaningLines[1])
                            # if not case:
                            #     continue
                            #
                            # definitePerson = getDef(meaningLines[1])
                            # if definitePerson:
                            #     if case + 'def' not in globalDict[directForm][number]:
                            #         globalDict[directForm][number][case + 'def'] = {'person1': [], 'person2': [],'person3': []}
                            #     if wordTxt not in globalDict[directForm][number][case + 'def'][definitePerson]:
                            #         globalDict[directForm][number][case + 'def'][definitePerson].append(wordTxt)
                            # else:
                            #     if case not in globalDict[directForm][number]:
                            #         globalDict[directForm][number][case] = []
                            #     if wordTxt not in globalDict[directForm][number][case ]:
                            #         globalDict[directForm][number][case].append(wordTxt)




                            # if (len(globalDict.keys()) >= 10):
                            #    break

import json

with open('/home/vahagn/arevak.dic', 'w', encoding='utf8') as file:
    file.write(json.dumps(globalDict, ensure_ascii=False))

exit(0)
print(globalDict)
exit(0)


def compareDecls(our, their):
    found = False
    for number in ['plural', 'singular']:
        for case in our[number]:
            if case in their[number] and len(their[number][case]) and our[number][case] not in their[number][case]:
                return False
    return True


arml = armlib.NounTransformations
for word in globalDict:
    if word == 'բավականություն':
        pass
    for transformation in arml:
        t = transformation.transform(word, type != 'inanim')
        if compareDecls(t, globalDict[word]):
            globalDict[word]["alloweddecl"].append(transformation.name)
print('Found {0} words', len(globalDict.keys()))

with open('result.csv', 'w') as file:
    for word in globalDict:
        file.write('"{0}",'.format(word) + ','.join(globalDict[word]['alloweddecl']) + '\n')
