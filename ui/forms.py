from django import forms

class LoginForm(forms.Form):

    username = forms.CharField(max_length=200)
    password = forms.CharField(widget=forms.PasswordInput())

    username.widget.attrs['class'] = 'form-control'
    username.widget.attrs['placeholder'] = 'Username'

    password.widget.attrs['class'] = 'form-control'
    password.widget.attrs['placeholder'] = 'Username'


