# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-17 09:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ui', '0003_rawword_wordoccurence'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rawword',
            name='Word',
            field=models.CharField(db_index=True, max_length=200, unique=True),
        ),
    ]
