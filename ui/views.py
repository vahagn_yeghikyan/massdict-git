from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse,HttpResponseNotAllowed
from django.template import loader,RequestContext
from . import forms as localForms
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.conf import settings
# Create your views here.
def index(request):
    #if not (request.user and request.user.is_authenticated()):
    #    return login(request)

    tpl = loader.get_template('base.html')
    return HttpResponse(tpl.render())

def login(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            return HttpResponseRedirect('/index/')
        else:
            loginForm = localForms.LoginForm()
            context = RequestContext(request,{'loginform':loginForm})
            tpl = loader.get_template('login.html')
            return HttpResponse(tpl.render(context))
    elif request.method == 'POST':

        loginform = localForms.LoginForm(request.POST)
        if loginform.is_valid():
            user = authenticate(username= loginform.cleaned_data['username'], password= loginform.cleaned_data['password'])
            if user:
                django_login(request, user)
                return HttpResponseRedirect(settings.LOGIN_FORM_REDIRECT)

    else:
        return HttpResponseNotAllowed(['GET','POST'])

