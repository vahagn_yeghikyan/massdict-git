from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Dictionary(models.Model):
    Word = models.CharField(max_length=200, db_index=True)
    Part = models.CharField(max_length=20)
    Transformation = models.CharField(max_length=50, null=True)
    Parent = models.ForeignKey("self", null=True)
    Person = models.CharField(max_length=20, null=True)
    Number = models.CharField(max_length = 8, null=True)
    SubmitUser = models.ForeignKey(User)

class Article(models.Model):
    Content = models.TextField(null=True)
    Author = models.CharField(max_length=200,null=True)
    Url = models.CharField(max_length=500)
    ProtalId = models.IntegerField()
    Views = models.IntegerField(null=True)
    PublicationDate = models.DateField()
    PublicationTime = models.TimeField()
    CrawlDate = models.DateTimeField()
    Title = models.CharField(max_length=600)
    SiteId = models.IntegerField(null=True)
    State = models.IntegerField()


class RawWord(models.Model):
    Word = models.CharField(max_length=200, db_index=True, unique=True)


class WordOccurence(models.Model):
    Word = models.ForeignKey(RawWord)
    Article = models.ForeignKey(Article)